import { Router, ActivatedRoute } from '@angular/router';
import { DataPassService } from './../data-pass.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  details;

  constructor(private passData: DataPassService, private router : Router , private activatedRoute : ActivatedRoute) {
    // this.passData.userPass.subscribe(
    //   (user)=>{
    //      this.details.name = user['name'];
    //      this.details.email = user['email'];
    //      this.details.contact = user['contact'];
    //      this.details.password = user['password'];
    //      alert(this.details.name);
    //   }   );
  }

  ngOnInit() {
      this.details = this.passData.getUserData();
     // alert(this.details);
  }

  admin(){
    if(this.details.email === 'mishra.sagar25@gmail.com') {
      this.router.navigate(['users'], {relativeTo : this.activatedRoute});
    }
    else
    {
      alert("To view all users you must have admin rights");
    }

  }


}
