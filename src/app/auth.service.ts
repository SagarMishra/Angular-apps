import { Router } from '@angular/router';
import { AccountsService } from './accounts.service';
import { Injectable } from '@angular/core';
import { DbHelperService } from './db-helper.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private loggedIn = false;

  constructor(private accountService: AccountsService, private db: DbHelperService) { }

  isAuthenticate(){
    const promise = new Promise(
      (resolve,reject) =>{
        setTimeout(() => {
          resolve(this.loggedIn);
        },100)
      }
    );
    return promise;
  }

  login() {
      this.loggedIn = true;

  }

  logout(){
    this.loggedIn = false;
  }
}
