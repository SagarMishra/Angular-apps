import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class DataPassService {
  userPass = new Subject();
  users:any;
  constructor() { }
  user : any;
  name : string;
  email : string;
  contact : string;
  password : string;
  gender : string;
  setUserData(user : any){
    this.user = user;
    this.name = this.user.name;
    this.email = this.user.email;
    this.contact = this.user.contact;
    this.password = this.user.password;
    this.gender = this.user.gender;

    //alert(this.user.name);
  }

  getUserData() {
    return {name : this.name , email : this.email , contact : this.contact, password : this.password, gender : this.gender};
  }

  setUsers(data : any){
    this.users = data;
    //alert(this.users[1].name);
  }

  getUsers(){
    return this.users;
  }
}
