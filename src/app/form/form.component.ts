import { Router } from '@angular/router';
import { AccountsService } from './../accounts.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm, FormGroup, FormControl, Validators } from '@angular/forms';
import { CanComponentDeactivate } from '../can-deactivate-guard.service';
import { Observable } from '../../../node_modules/rxjs';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit,CanComponentDeactivate {
  genders = ['male','female'];
  defaultGender = 'male';
  signUpForm: FormGroup;
  checkForm = false;
  forbidNames = ['Sagar Mishra', 'sagar mishra'];
  name;
  email;
  contact;
  pwd;
  gender;
  user = {
    name : '',
    email : '',
    contact : '',
    pwd : '',
    gender : '',
  };
  constructor(private accountService: AccountsService, private router : Router) { }

  ngOnInit() {
    this.signUpForm = new FormGroup({
      'name' : new FormControl(null, [Validators.required/*, this.forbiddenNames.bind(this)*/]),
      'email' : new FormControl(null, [Validators.required , Validators.email]),
      'contact' : new FormControl(null, [Validators.required]),
      'password' : new FormControl(null, [Validators.required]),
      'gender' : new FormControl('male')
    });
  }

  forbiddenNames(control: FormControl): {[s: string ]: boolean}{
    if(this.forbidNames.indexOf(control.value) !== -1)
      {
        return {'nameIsForbidden' : true};
      }
      return null;
  }


  onSubmit() {
    this.name = this.signUpForm.value.name;
    this.email = this.signUpForm.value.email;
    this.contact = this.signUpForm.value.contact;
    this.pwd = this.signUpForm.value.password;
    this.gender = this.signUpForm.value.gender;
    this.accountService.register(this.name, this.email, this.contact, this.pwd, this.gender);
    this.signUpForm.reset();
    this.router.navigate(['/login']);
    //this.checkForm = true;
  }

  canDeactivateMyComponent() : Observable<boolean> | Promise<boolean> | boolean{
    if(this.name === '' || this.email === '' || this.contact === '' || this.pwd === '' || this.gender == '')
    {
      alert("aap phle register kro");
    } else {
      return true;
    }
  }
}
