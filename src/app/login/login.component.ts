import { DataPassService } from './../data-pass.service';
import { AccountsService } from './../accounts.service';
import { NgForm } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '../../../node_modules/@angular/router';
import { DbHelperService } from '../db-helper.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  details;
  @ViewChild('f') loginForm: NgForm;
  constructor(private db : DbHelperService, private authService: AuthService,
    private accountsService : AccountsService, private router : Router,
    private passdata : DataPassService
  ) { }

  ngOnInit() {
  }

  onSubmit(){
    //this.accountsService.login(this.loginForm.value.email, this.loginForm.value.password);

    this.db.getUsers()
    .subscribe(
      (response: any) => {
        let found = false;
        this.details = response;
        let users :{name : string, email : string, contact : string , password :string , gender: string}[] = [];

        //console.log(this.details);
        for (let key in this.details) {
          if (this.details.hasOwnProperty(key)) {

            const val = this.details[key];
            users.push({name : val['name'], email : val['email'], contact : val['contact'], password : val['password'], gender : val['gender'] });
            //alert(val['email'] + ", " + val['password']);
            if ( val['email'] === this.loginForm.value.email && val['password'] === this.loginForm.value.password){
                document.getElementById('dash').style.display = 'flex';
                document.getElementById('out').style.display = 'flex';
                found = true;
                this.authService.login();
                this.passdata.userPass.next(val);
                let userd = {
                  name : val['name'],
                  email : val['email'],
                  contact : val['contact'],
                  password : val['password'],
                  gender : val['gender']
                };
                // tslint:disable-next-line:max-line-length

                this.passdata.setUserData(userd);
                this.router.navigate(['/dashboard']);
            }
          }
        }
        this.passdata.setUsers(users);
        if(!found) {
          alert("user not found");
        }
      }
    );
  }
}
