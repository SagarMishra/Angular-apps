import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WebSeriesComponentComponent } from './web-series-component.component';

describe('WebSeriesComponentComponent', () => {
  let component: WebSeriesComponentComponent;
  let fixture: ComponentFixture<WebSeriesComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WebSeriesComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WebSeriesComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
