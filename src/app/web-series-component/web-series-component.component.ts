import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-web-series-component',
  templateUrl: './web-series-component.component.html',
  styleUrls: ['./web-series-component.component.css']
})
export class WebSeriesComponentComponent  {
  title = 'Web Series App';
  series = [
    {name : 'Sacred Games', channel : 'Netflix'},
    {name : 'Got', channel : 'HBO'}
  ];

  name: string;
  channel: string;
  index: number;
  onRemove(i) {
    this.series.splice(i, 1);
  }

  onEdit(i) {
    this.name = this.series[i].name;
    this.channel = this.series[i].channel;
    this.index = i;
  }

  onEditDone(i) {
    this.series[i].name = this.name;
    this.series[i].channel = this.channel;
    this.name = '';
    this.channel = '';
    this.index = null;
  }

  onAdd() {
    this.series.push({name : this.name , channel : this.channel});
    this.name = '';
    this.channel = '';
  }
  constructor() { }


}
