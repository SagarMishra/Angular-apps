import { CanComponentDeactivate } from './can-deactivate-guard.service';
import { Injectable } from '@angular/core';
import { Observable } from '../../node_modules/rxjs';
import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '../../node_modules/@angular/router';

export interface CanComponentDeactivate {
  canDeactivateMyComponent: () => Observable<boolean> | Promise<boolean> | boolean;
}
@Injectable({
  providedIn: 'root'
})
export class CanDeactivateGuardService implements CanDeactivate<CanComponentDeactivate>{

  constructor() { }

  canDeactivate(component: CanComponentDeactivate,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean{
        return component.canDeactivateMyComponent();
    }


}
