import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class DbHelperService {

  constructor(private http: Http) { }

  storeUser(user: any) {
    return this.http.post('https://angular-apps-9e29b.firebaseio.com/users.json', user);
  }

  getUsers() {
    return this.http.get('https://angular-apps-9e29b.firebaseio.com/users.json').pipe(
      map(
        (response: Response)  => {
          const data = response.json();
          return data;
        }
      )
    );


  }
}
