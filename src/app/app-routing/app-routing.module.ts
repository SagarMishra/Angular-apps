import { PageNotFoundComponentComponent } from './../page-not-found-component/page-not-found-component.component';
import { FormComponent } from './../form/form.component';
import { DetailsComponentComponent } from './../details-component/details-component.component';
import { WebSeriesComponentComponent } from './../web-series-component/web-series-component.component';
import { NewCompComponent } from './../new-comp/new-comp.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

const appRoutes: Routes = [
  {path : '' , component : NewCompComponent},
  {path: 'series', component : WebSeriesComponentComponent},
  {path: 'details', component : DetailsComponentComponent},
  {path: 'signUp', component : FormComponent},
  {path : 'not-found', component : PageNotFoundComponentComponent },
  {path : '**' , redirectTo : '/not-found'}
];
@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports : [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
