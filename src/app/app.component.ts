import { Router, RouterLinkWithHref } from '@angular/router';
import { DbHelperService } from './db-helper.service';
import { Component } from '@angular/core';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'First Angular App';
  num = 10;
  parentCount = 0;
  name: string;
  fname: string;
  lname: string;
  constructor(private auth: AuthService, private router : Router){}
  case() {

    if (this.isUpperCase(this.name)) {
      this.name = this.name.toLowerCase();
    } else {
      this.name = this.name.toUpperCase();
    }


  }

  isUpperCase(str) {
    return str === str.toUpperCase();
  }

  onIncrement(val: number) {
    this.parentCount = val;
  }


  showDetails(event) {
    this.fname = event.fname;
    this.lname = event.lname;
  }

  logout(){
    this.auth.logout();
    this.router.navigate(['/']);
  }
}
