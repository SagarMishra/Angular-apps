import { Component, OnInit, ContentChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-new-comp',
  templateUrl: './new-comp.component.html',
  styleUrls: ['./new-comp.component.css']
})
export class NewCompComponent implements OnInit {

  title = 'Bucket List App';
  bucketList = ['lohagad', 'Rajmachi', 'Goa'];
  isValid = true;
  selectedItem: string;
  insertItem: string;
  test = 'testing child component';
  @ContentChild('pname') parentname: ElementRef;

  constructor(private router: Router, private route: ActivatedRoute) { }

  onLoadDetails() {
    this.router.navigate(['/details'], {relativeTo : this.route});
  }
  AddItem(): void {
    if (!this.bucketList.includes(this.insertItem)) {
      this.bucketList.push(this.insertItem);

    } else {
      alert(this.insertItem + ' already present in bucket list');
    }
    this.insertItem = '';

  }
  onItemClicked(sItem) {
    this.selectedItem = sItem;
  }
  myFunction() {
    alert('you clicked the fucking button');
  }

  onToggle() {
    if (this.isValid === true) {
      this.isValid = false;
    } else {this.isValid = true;
    }
  }
  onRemoveItem() {
    if (this.bucketList.includes(this.selectedItem)) {
      const index: number = this.bucketList.indexOf(this.selectedItem);
      this.bucketList.splice(index, 1);
      this.selectedItem = '';
    }

  }


  ngOnInit() {
  }

}
