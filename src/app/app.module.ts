import { DataPassService } from './data-pass.service';
import { AuthGuardService } from './auth-guard.service';
import { AuthService } from './auth.service';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { AccountsService } from './accounts.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { NewCompComponent } from './new-comp/new-comp.component';
import { CounterComponentComponent } from './counter-component/counter-component.component';
import { DetailsComponentComponent } from './details-component/details-component.component';
import { ReplaceDirectiveDirective } from './replace-directive.directive';
import { WebSeriesComponentComponent } from './web-series-component/web-series-component.component';
import { BetterHighlightDirective } from './better-highlight.directive';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponentComponent } from './page-not-found-component/page-not-found-component.component';
import { FormComponent } from './form/form.component';
import { LoginComponent } from './login/login.component';
import { CanDeactivateGuardService } from './can-deactivate-guard.service';
import { DbHelperService } from './db-helper.service';
import { HttpModule } from '@angular/http';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UsersComponent } from './users/users.component';
const appRoutes: Routes = [
  {path : '' , component : FormComponent},
  {path : 'bucket' , canActivate : [AuthGuardService], component : NewCompComponent},
  {path: 'series', canActivate : [AuthGuardService], component : WebSeriesComponentComponent},
  {path: 'details', component : DetailsComponentComponent},
  {path: 'signup', component : FormComponent , canDeactivate : [CanDeactivateGuardService]},
  {path: 'login' , component : LoginComponent},
  {path : 'dashboard', component : DashboardComponent , children : [
    {path : 'users' , component : UsersComponent}
  ]},
  {path : 'not-found', component : PageNotFoundComponentComponent },
  {path : '**' , redirectTo : '/not-found'}
];

@NgModule({
  declarations: [
    AppComponent,
    NewCompComponent,
    CounterComponentComponent,
    DetailsComponentComponent,
    ReplaceDirectiveDirective,
    WebSeriesComponentComponent,
    BetterHighlightDirective,
    PageNotFoundComponentComponent,
    FormComponent,
    LoginComponent,
    DashboardComponent,
    UsersComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
    HttpModule

  ],
  providers: [DataPassService, DbHelperService, AccountsService, AuthService, AuthGuardService, CanDeactivateGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
