import { Directive, ElementRef, Renderer2, HostListener, HostBinding } from '@angular/core';
import { MockNgModuleResolver } from '../../node_modules/@angular/compiler/testing';

@Directive({
  selector: '[appBetterHighlight]'
})
export class BetterHighlightDirective {

  @HostBinding('style.backgroundColor') backgroundColor = 'transparent';
  @HostBinding('style.color') color = 'black';

  constructor(private elementRef: ElementRef, private renderer: Renderer2) { }

  @HostListener('mouseenter') mouseover(event: Event) {
    this.renderer.setStyle(this.elementRef.nativeElement, 'background-color', 'blue');
    this.renderer.setStyle(this.elementRef.nativeElement, 'color', 'white');
}

@HostListener('mouseleave') mouseleave(event: Event) {
  this.renderer.setStyle(this.elementRef.nativeElement, 'background-color', 'inherit');
  this.renderer.setStyle(this.elementRef.nativeElement, 'color', 'inherit');
}

}
