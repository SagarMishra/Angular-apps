import { Component, OnInit, EventEmitter, Input, Output, ViewChild, ElementRef } from '@angular/core';


@Component({
  selector: 'app-counter-component',
  templateUrl: './counter-component.component.html',
  styleUrls: ['./counter-component.component.css']
})
export class CounterComponentComponent implements OnInit {
  @Input() count;
  @Output() result = new EventEmitter<number>();
  @ViewChild('name') servName: ElementRef;
  increment() {
    this.count++;
    this.result.emit(this.count);
  }
  constructor() { }
  ngOnInit() {
  }

  show() {
    window.alert(this.servName.nativeElement.value);
  }

}
