import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-details-component',
  templateUrl: './details-component.component.html',
  styleUrls: ['./details-component.component.css']
})
export class DetailsComponentComponent implements OnInit {
  fname = '';
  lname = '';

  @Output() details = new EventEmitter<any>();

  passDetails() {
    this.details.emit({fname : this.fname, lname : this.lname});
    this.fname = '';
    this.lname = '';
  }

  constructor() { }

  ngOnInit() {
  }

}
