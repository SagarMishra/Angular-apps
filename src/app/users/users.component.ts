import { DataPassService } from './../data-pass.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users :any;

  constructor(private db:DataPassService) { }

  ngOnInit() {
    this.users = this.db.getUsers();
   // alert(this.users[1].name);
  }



}
